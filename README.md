**Apps**

Web-based application managed by PT. Perkebunan Nusantara X (PTPN X) which provides a place for the whole community to be able to invest in the sugar cane agriculture sector


**Dockerfile**

In this Dockerfile contains the image intended for PHP and MySQL development. For convenience, it also runs SSH server to connect to. Both MySQL and phpmyadmin use default XAMPP password.
Mount /InvestaniCrowdfunding, URL: http://localhost:[port]/InvestaniCrowdfunding